package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import java.util.Collections;

public class Main {

	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}

		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		// get container
		DOMController domController = new DOMController(xmlFileName);
		Flowers flowers = domController.loadData();

		// sort (case 1)
		Collections.sort(flowers.getFlowers(),
				domController.compareFlowerByName);
		// save
		String outputXmlFile = "output.dom.xml";
		domController.setXmlFileName(outputXmlFile);
		domController.saveToXml(flowers);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////

		// get
		SAXController saxController = new SAXController(xmlFileName);

		flowers = saxController.loadData();

		// sort  (case 2)
		Collections.sort(flowers.getFlowers(),
				new DOMController("").compareFlowerBySoil);

		// save
		outputXmlFile = "output.sax.xml";
		saxController.setXmlFileName(outputXmlFile);
		saxController.writeToXml(flowers);

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////

		// get
		STAXController staxController = new STAXController(xmlFileName);
		flowers = staxController.loadData();

		// sort  (case 3)
		Collections.sort(flowers.getFlowers(),
				new DOMController("").compareFlowerByOrigin);

		// save
		outputXmlFile = "output.stax.xml";
		staxController.setXmlFileName(outputXmlFile);
		staxController.saveToXml(flowers);
	}

}

