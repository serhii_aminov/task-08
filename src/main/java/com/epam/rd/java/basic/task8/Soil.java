package com.epam.rd.java.basic.task8;

import java.util.Arrays;

public enum Soil {
    PODZOLIC("подзолистая"),
    UNPAVED("грунтовая"),
    SODPODZOLIC("дерново-подзолистая");

    private String title;

    static public Soil getByValue(String value){
        return Arrays.stream(Soil.values())
                .filter(soil -> soil.getTitle().equals(value))
                .findFirst().get();
    }

    Soil(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return getTitle();
    }

}

