package com.epam.rd.java.basic.task8;

import java.util.Arrays;

public enum Multiplying {
    LEAVES("листья"),
    CUTTINGS("черенки"),
    SEEDS("семена");

    private String title;

    static public Multiplying getByValue(String value){
        return Arrays.stream(Multiplying.values())
                .filter(soil -> soil.getTitle().equals(value))
                .findFirst().get();
    }


    Multiplying(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return getTitle();
    }
}

